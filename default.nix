{ nixpkgs }:

with nixpkgs; {
  tracecompass = callPackage ./tracecompass {};
  logseq = callPackage ./logseq {};
}
