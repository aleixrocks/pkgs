final: prev: {
  tracecompass = final.callPackage pkgs/tracecompass/fod.nix {};
  python-ariadna = final.python310.withPackages (pythonPackages: with pythonPackages; [
      pyyaml
      cerberus
      pandas
      seaborn
      gitpython
  ]);

  #openjfx22-webkit = prev.openjfx22.override { withWebKit = true; ffmpeg_4 = final.ffmpeg_5-full; };
  openjfx22-webkit = prev.openjfx22.override { withWebKit = true;  };
  jdk22-fx = prev.jdk22.override { enableJavaFX = true; openjfx = final.openjfx22-webkit; };

  openjfx21-webkit = prev.openjfx21.override { withWebKit = true;  };
  jdk21-fx = prev.jdk21.override { enableJavaFX = true; openjfx = final.openjfx21-webkit; };

  openjfx-webkit = prev.openjfx.override { withWebKit = true;  };
  jdk-fx = prev.jdk.override { enableJavaFX = true; openjfx = final.openjfx-webkit; };
}
