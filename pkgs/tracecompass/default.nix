{ lib, fetchurl, jre, makeWrapper, maven }:

maven.buildMavenPackage rec {
  pname = "tracecompass";
  version = "9.2.0";

  src = fetchurl {
    url = "https://git.eclipse.org/c/tracecompass/org.eclipse.tracecompass.git/snapshot/org.eclipse.tracecompass-${version}.tar.gz";
    hash = "sha256-sb0rPmThKKWOx5H0hO/bVaueVTPvb/0iZn8kU4EORKU=";
  };

  mvnHash = "";

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir -p $out/bin $out/share/jd-cli
    install -Dm644 jd-cli/target/jd-cli.jar $out/share/jd-cli

    makeWrapper ${jre}/bin/java $out/bin/jd-cli \
      --add-flags "-jar $out/share/jd-cli/jd-cli.jar"
  '';

  meta = with lib; {
    description = "Linux tool for visualizing traces";
    homepage = "https://eclipse.dev/tracecompass/";
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [ arks ];
  };
}
