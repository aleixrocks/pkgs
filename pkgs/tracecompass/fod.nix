{ 
  stdenv
, lib
, fetchurl
, jre
, makeWrapper
, wrapGAppsHook
, autoPatchelfHook
}:

stdenv.mkDerivation {
  pname = "tracecompass";
  version = "9.2.0";

  src = fetchurl {
    url = "https://www.eclipse.org/downloads/download.php?file=/tracecompass/releases/9.2.0/rcp/trace-compass-9.2.0-20231207-0916-linux.gtk.x86_64.tar.gz&r=1";
    hash = "sha256-V2Iu0dkekYAWSCKJBXXxVs/FBUk69dggf4qEs5cKr9E=";
  };

  dontConfigure = true;
  dontBuild = true;

  nativeBuildInputs = [
    autoPatchelfHook
    wrapGAppsHook
    makeWrapper
  ];

  propagatedBuildInputs = [
    jre
  ];

  installPhase = ''
    mkdir -p $out/bin $out/share/java
    #makeWrapper ${jre}/bin/java $out/bin/tracecompass
    cp -r * $out/share/java
    makeWrapper $out/share/java/tracecompass $out/bin/tracecompass  \
      --prefix PATH : "${lib.makeBinPath [ jre ]}"
  '';

  meta = with lib; {
    description = "Tool for visualizing traces";
    homepage = "https://eclipse.dev/tracecompass/";
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [ arks ];
  };
}
