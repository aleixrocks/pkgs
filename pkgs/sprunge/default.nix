{buildPythonPackage}:

buildPythonPackage {
  preBuild = ''
    cat > setup.py << EOF
from setuptools import setup

#with open('requirements.txt') as f:
#    install_requires = f.read().splitlines()

setup(
  name='sprunge',
  #packages=['someprogram'],
  version='0.1.0',
  #author='...',
  #description='A script for fetching or posting text pastes on http://sprunge.us.',
  install_requires=install_requires,
  scripts=[
    'sprunge',
  ],
  entry_points={
    # example: file some_module.py -> function main
    #'console_scripts': ['someprogram=some_module:main']
  },
)
    EOF
  '';
}

