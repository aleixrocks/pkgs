{
  description = "A very basic flake";

  inputs = {
    #nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    #nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    bscpkgs.url = "git+https://jungle.bsc.es/git/rarias/bscpkgs";
    bscpkgs.inputs.nixpkgs.follows = "system";
  };
  nixConfig.bash-prompt = "\[nix-develop\]$ ";

  outputs = { self, system, bscpkgs }:
  let
    overlay = import ./overlay.nix;
    bscOverlay = bscpkgs.bscOverlay;
    pkgs = import system {
      system = "x86_64-linux";
      overlays = [ bscOverlay overlay ];
    };
  in {
    #packages.x86_64-linux = import ./default.nix { nixpkgs = pkgs.legacyPackages.x86_64-linux; };
    #packages.x86_64-linux = import ./default.nix { nixpkgs = pkgs; };
    packages.x86_64-linux = pkgs;
  };
}
